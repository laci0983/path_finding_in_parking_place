%This project performs the planning of the parking lot traversal based on
%the optimization of the airline based distance. The algorithm requires two
% binary images: tha map of the road surface and the map of the parking
% zones. The output of the algorithm is the sequence of points along the
% Voronoi diagram of the road surface

clear all
close all
clc

global draw;
draw=1;

%video variables
global makeVideo;
makeVideo=0;
global numOfFrame;
numOfFrame=1;


mapFileName='occupancy_grid.mat';
[parkingPlaces,mapOfParkingLot] = readMap(mapFileName);

%%skeleton is produces for detection
skeletonOfParkingLot=bwskel(mapOfParkingLot);
if draw==1
    figure;
    imshow(skeletonOfParkingLot)
end



%%start point coordinates
c1=441;
r1=1849;
%startPoint=[533 544]
startPoint=[c1 r1];
[pointsToReachInSequence,centerPointsOfZones] = getParkingZoneCoordinates(parkingPlaces,skeletonOfParkingLot,startPoint);



crossings = getRoadCrossings(mapOfParkingLot);


[G,DG,nodeVector] = createGraphOfParkingLot(pointsToReachInSequence,crossings,mapOfParkingLot);



figure;
imshow(~skeletonOfParkingLot);
hold on
p=plot(G,'XData',nodeVector(:,1),'YData',nodeVector(:,2),'NodeLabel',{});
p.Marker='s';
p.NodeColor='r';
p.MarkerSize = 7;
p.EdgeColor='green';
p.LineWidth=4;
hold on
plot(crossings(:,1),crossings(:,2),'b*','MarkerSize',10,'LineWidth',2);

%appoint only nodes, that are parking zone point
isPointToReach=[];
for i=1:size(nodeVector,1)
    node=nodeVector(i,:);
 [isNodeCrossing,~] = ismember(node,crossings,'rows');
 if(isNodeCrossing)
     isPointToReach(end+1,:)=0;
 else
    isPointToReach(end+1,:)=1;
 end
end

finalPathWithAllPoints=[];
for i=1:size(pointsToReachInSequence,1)-1
    start=pointsToReachInSequence(i,:);
    pointToReach=pointsToReachInSequence(i+1,:);
    %%finding the shortest path between 2 points
    D1 = bwdistgeodesic(skeletonOfParkingLot, start(1), start(2), 'quasi-euclidean');
    D2 = bwdistgeodesic(skeletonOfParkingLot, pointToReach(1), pointToReach(2), 'quasi-euclidean');
    D = D1 + D2;
    D = round(D * 8) / 8;
    
    D(isnan(D)) = inf;
    skeleton_path = imregionalmin(D);
    
    %%getting all coordinates of the path
    [row col]=find(skeleton_path==1);
    cords=[row col];
    cords( ismember(cords, start, 'rows'), :) = [];
    pointTofind=start;
    
    %%getting the sequence of path coordinates by minimizing the distance
    finalPathInOrder = findSequenceOfPathCoordinates(cords,start);
    
    
    finalPathWithAllPoints=[finalPathWithAllPoints;finalPathInOrder];
    

end


%Drawing final route
if draw==1
    figure
    imshow(~skeletonOfParkingLot);
    hold on
    plot(finalPathWithAllPoints(:,2),finalPathWithAllPoints(:,1),'gx');
    hold on
    plot(pointsToReachInSequence(1,1),pointsToReachInSequence(1,2),'bo','MarkerSize',20);
    hold on
    plot(pointsToReachInSequence(2:end,1),pointsToReachInSequence(2:end,2),'r*','MarkerSize',20);
end




%creating video file
if(makeVideo)
    % create the video writer with 1 fps
    writerObj = VideoWriter('myVideo.avi');
    writerObj.FrameRate = 1;
    % set the seconds per image
    % open the video writer
    open(writerObj);
    % write the frames to the video
    for i=1:length(F)
        % convert the image to a frame
        frame = F(i) ;
        writeVideo(writerObj, frame);
    end
    % close the writer object
    close(writerObj);
end



