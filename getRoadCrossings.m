function branchCoordinates = getRoadCrossings(image)
% This function finds the branchpoints of the road surface, that are the
% road crossings
%@param:
%         image - binary image of the road surface
%@return:
%         branchCoordinates - coordinates of the road crossings
%                             Size: Nx2
skel=bwskel(image);
B =  bwmorph(skel,'branchpoints');
[y,x]=find(B);

branchCoordinates=[x,y];
end

