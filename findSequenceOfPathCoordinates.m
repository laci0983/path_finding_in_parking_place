function sequenceOfCoordinates = findSequenceOfPathCoordinates(coordinates,initialPoint)
% This function provides the sequence of coordinates that minimizes the
% airline based distance between the points. From the initial point the
% algorithm selects the closest point as the next point. This step is
% repeated recursively until all point are visited
%@param:
%           coordinates     -  coordinates of the points to be reached
%                              during the traversal
%                              Size: Nx2
%           initialPoint    -  start  coordinate of the traversal
%                              Size: 1x2
%@return:
%          sequenceOfCoordinates - coordinates sorted, where the airline
%                                  based distance is minimized starting
%                                  from the initial point
%                                  Size: Nx2
    pointTofind=initialPoint;
    sequenceOfCoordinates=[];

    while size(coordinates,1)>0
        
        %compute Euclidean distances:
        distances = sqrt(sum(bsxfun(@minus, coordinates, pointTofind).^2,2));
        %find the smallest distance and use that as an index into B:
        closest = coordinates(find(distances==min(distances)),:);
        
        sequenceOfCoordinates(end+1,:)=closest(1,:);
        pointTofind=closest(1,:);
        coordinates( ismember(coordinates, pointTofind, 'rows'), :) = [];
    end
end

