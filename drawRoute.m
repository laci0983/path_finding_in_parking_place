function drawRoute(skel,skeleton_path,startPoint,pointToReach)
global makeVideo;
figure
    P = imoverlay(skel, imdilate(skeleton_path, ones(3,3)), [1 0 0]);
    imshow(P, 'InitialMagnification', 200);
    hold on
    plot(startPoint(1), startPoint(2), 'go', 'MarkerSize', 15)
    plot(pointToReach(1), pointToReach(2), 'c*', 'MarkerSize', 15)
    title('Route between parking zones. o - start , * - goal')
    if(makeVideo)
        global numOfFrame
        F(numOfFrame) = getframe(gcf) ;
        numOfFrame=numOfFrame+1;
        drawnow
    end

end

