function [G,DG,nodeVector] = createGraphOfParkingLot(pointsToReach,crossings,mapOfParkingLot)
%This function constructs the graph of the parking lot from the closest
%points of the Voronoi diagram to the parking zone centroids, the road
%crossings. The map of the parking lot is required to assign weights to the
%edges of the graph representing the physical distance between these
%points.
%@param:
%           pointsToReach   -  points of the Voronoi diagram that are to be
%                              reached during the exploration as they are 
%                              the closest points to the parking zone 
%                              centroids
%                              Size: N x 2  matrix
%           crossings       - road crossing coordinates
%                             Size: N_c x 2 matrix
%           mapOfParkingLot - binary image of the road surface
%@return:
%          G                - Graph where all roads are considered as
%                             bidirectional
%                             Size: 1x1 graph
%          DG               - Directed graph
%                             Size: 1x1 digraph
%          nodeVector       - nodes of the graph constructed
%                             Size: (N_c+N) x 2 matrix



global draw;

nodes=[];
skeletonOfParkingLot=bwskel(mapOfParkingLot);
for i=1:size(pointsToReach)
    skeletonOfParkingLot2=bwskel(mapOfParkingLot);
    parkingZonePoint=pointsToReach(i,:);
    %geodesic distance from initial point points
    D1=bwdistgeodesic(skeletonOfParkingLot2,parkingZonePoint(1),parkingZonePoint(2),'quasi-euclidean');
    
    y1=[];
    x1=[];
    distCross1=inf;
    for j=1:size(crossings,1)
        dist=D1(crossings(j,2),crossings(j,1));
        if(dist<distCross1)
            distCross1=dist;
            [y1,x1]=find(D1==distCross1);
        end
    end
    XY=[x1,y1];
    XY=XY(ismember(XY,crossings,'rows'),:);
    x1=XY(1,1);
    y1=XY(1,2);
    
    %delete path between these points
    D2 = bwdistgeodesic(skeletonOfParkingLot2, x1, y1, 'quasi-euclidean');
    D = D1 + D2;
    D = round(D * 8) / 8;
    D(isnan(D)) = inf;
    skeleton_path = imregionalmin(D);
    %%getting all coordinates of the path
    [row,col]=find(skeleton_path==1);
    %deletion
    skeletonOfParkingLot2(row,col)=0;
    %restore road crossings
    skeletonOfParkingLot2(y1,x1)=1;
    %restore parking lot point
    skeletonOfParkingLot2(parkingZonePoint(2),parkingZonePoint(1))=1;
    
    D1=bwdistgeodesic(skeletonOfParkingLot2,parkingZonePoint(1),parkingZonePoint(2),'quasi-euclidean');

    y2=[];
    x2=[];
    distCross2=inf;
    for j=1:size(crossings,1)
        dist=D1(crossings(j,2),crossings(j,1));
        if(dist<distCross2)
            distCross2=dist;
            [y2,x2]=find(D1==distCross2);
        end
    end
    
    if(isempty(y2) | isempty(x2))
        y2=y1;
        x2=x1;
    end
    
    XY=[x2,y2];
    
    
    XY=XY(ismember(XY,crossings,'rows'),:);
    x2=XY(1,1);
    y2=XY(1,2);
    if(length(y2)==1)
        if(x1<x2)
            nodes(end+1,:)=[x1 y1 parkingZonePoint(1) parkingZonePoint(2) x2 y2 distCross1 distCross2];
        else
            nodes(end+1,:)=[x2 y2 parkingZonePoint(1) parkingZonePoint(2) x1 y1 distCross2 distCross1];
        end
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Add essential edges between crossing vertices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:size(crossings,1)
    crossingsToCheck=crossings;
    crossingsToCheck(i,:)=[];
    %get current crossing
    crossingToBind=crossings(i,:);
    skeletonOfParkingLot2=bwskel(mapOfParkingLot);
    %delete all parking lot points from map
    skeletonOfParkingLot2(pointsToReach(:,2),pointsToReach(:,1))=0;
    %%%%%%%%%% creating edges between closest critical vertices of
    %%%%%%%%%% crossings
    skeletonOfParkingLot3=skeletonOfParkingLot2;
    %geodesic distance from crossint to bind
    D1=bwdistgeodesic(skeletonOfParkingLot3,crossingToBind(1),crossingToBind(2),'quasi-euclidean');

    
    y_closest=[];
    x_closest=[];
    closestDist_1=inf;
    for j=1:size(crossingsToCheck,1)
        dist=D1(crossingsToCheck(j,2),crossingsToCheck(j,1));
        if(dist<closestDist_1)
            closestDist_1=dist;
            [y_closest,x_closest]=find(D1==closestDist_1);
        end
    end
    XY=[x_closest,y_closest];
    if(isempty(y_closest) | isempty(x_closest) | closestDist_1==inf) %if there is no route between nodes
        continue;      %it is a non critical edge between crossings
    end
    
    XY=XY(ismember(XY,crossingsToCheck,'rows'),:);
    x_closest=XY(1,1);
    y_closest=XY(1,2);
    
    
    %Adding new node
    node1ToBindTo=[x_closest,y_closest];
    %delete the closest node, and check if there is an other node
    skeletonOfParkingLot3(y_closest,x_closest)=0;
    %Check if there is a second node to bind to
    D2=bwdistgeodesic(skeletonOfParkingLot3,crossingToBind(1),crossingToBind(2),'quasi-euclidean');

    y_closest=[];
    x_closest=[];
    closestDist_2=inf;
    for j=1:size(crossingsToCheck,1)
        dist=D2(crossingsToCheck(j,2),crossingsToCheck(j,1));
        if(dist<closestDist_2)
            closestDist_2=dist;
            [y_closest,x_closest]=find(D1==closestDist_2);
        end
    end
    
    if(isempty(y_closest) |  isempty(x_closest) | closestDist_2==inf ) %if there is no route between nodes
        y_closest=crossingToBind(2);
        x_closest=crossingToBind(1);
        closestDist_2=0;
    else
        XY=[x_closest,y_closest];
        XY=XY(ismember(XY,crossingsToCheck,'rows'),:);
        x_closest=XY(1,1);
        y_closest=XY(1,2);
    end
    
    
    
    node2ToBindTo=[x_closest,y_closest];
    
    nodes(end+1,:)=[node1ToBindTo(1,:),crossingToBind(1,:),node2ToBindTo(1,:),closestDist_1,closestDist_2];
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Improve graph
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% node= [fist_node(x,y)  second_node(x,y)  third_node(x,y)  dist12  dist23]
for i=1:size(nodes,1)
    
    %middle point is always fixed
    middleNode=nodes(i,3:4); %second_node(x,y)
    if(ismember(middleNode,crossings))
        continue;
    end
    %fix left side first, than improve right side
    leftNode=nodes(i,1:2); %fist_node(x,y)
    %improve right side first, than fix the new right side
    rightNode=nodes(i,5:6);
    %original distance
    distLM=nodes(i,7); %dist12
    distMR=nodes(i,8); %dist23
    
    
    %%%%%%%%%%%%%%%%%%%%%%%
    % Init nodesToCheck array  -add to function
    %%%%%%%%%%%%%%%%%%%%%%%
    nodesToCheck=nodes(:,1:6);
    nodesToCheck(i,:)=[]; %delete actually observed input
    %[Nsect,Nvar]=size(nodesToCheck);
    nodesToCheck=[nodesToCheck(:,1:2);nodesToCheck(:,3:4);nodesToCheck(:,5:6)];
    %delete same nodes
    TF1 = ismember(nodesToCheck,leftNode,'rows');
    TF2 = ismember(nodesToCheck,middleNode,'rows');
    TF3= ismember(nodesToCheck,rightNode,'rows');
    TF= TF1 | TF2 | TF3;
    nodesToCheck(TF,:)=[];
    
    
    nodesToCheck1=nodesToCheck;
    %%%%%%%%%%%%%%%%%%%%%%%
    %right side improvement  - add to function
    %%%%%%%%%%%%%%%%%%%%%%%
    while(size(nodesToCheck1)>0)
        skeletonOfParkingLot2=bwskel(mapOfParkingLot);
        skeletonOfParkingLot2(leftNode(2),leftNode(1))=0;
        %geodesic distance from initial point points
        D1=bwdistgeodesic(skeletonOfParkingLot2,middleNode(1),middleNode(2),'quasi-euclidean');

        
        y_closest=[];
        x_closest=[];
        newDistance=inf;
        for j=1:size(nodesToCheck1,1)
            dist=D1(nodesToCheck1(j,2),nodesToCheck1(j,1));
            if(dist<newDistance)
                newDistance=dist;
                [y_closest,x_closest]=find(D1==newDistance);
            end
        end
        if(newDistance>distMR | isempty(y_closest) | isempty(x_closest))
            break;
        end
        XY=[x_closest,y_closest];
        XY=XY(ismember(XY,nodesToCheck1,'rows'),:);
        x_closest=XY(1,1);
        y_closest=XY(1,2);
        
        
        
        %deletion
        skeletonOfParkingLot2(y_closest,x_closest)=0;
        %Check if it changes path between two original
        D2=bwdistgeodesic(skeletonOfParkingLot2,middleNode(1),middleNode(2),'quasi-euclidean');
        distance=D2(rightNode(2),rightNode(1));
        %get path between there nodes
        D = D1 + D2;
        D = round(D * 8) / 8;
        D(isnan(D)) = inf;
        skeleton_path = imregionalmin(D);
        %%getting all coordinates of the path
        [y_path,x_path]=find(skeleton_path==1);
        path=[x_path y_path];
        
        %if it has changed the distance between the 2 orginial nodes,
        %and path is not going through left node -> improvement
        newRightNode=[x_closest,y_closest];
        if(distance~=distMR & ~isempty(newRightNode) & ~ismember(leftNode,path,'rows'))
            rightNode=newRightNode;
            distMR=newDistance;
            break;
            %else we delete the node and check for other possibilities
        else
            
            [~,ridx] = ismember(newRightNode,nodesToCheck1,'rows');
            if(ridx(1)>0)
                nodesToCheck1(ridx(1),:)=[];
            end
        end
    end
    
    
    
    nodesToCheck2=nodesToCheck;
    
    %%%%%%%%%%%%%%%%%%%%%%%
    %left side improvement -- add to function
    %%%%%%%%%%%%%%%%%%%%%%%
    while(size(nodesToCheck2)>0)
        skeletonOfParkingLot2=bwskel(mapOfParkingLot);
        skeletonOfParkingLot2(rightNode(2),rightNode(1))=0;
        %geodesic distance from initial point points
        D1=bwdistgeodesic(skeletonOfParkingLot2,middleNode(1),middleNode(2),'quasi-euclidean');
        
        y_closest=[];
        x_closest=[];
        newDistance=inf;
        for j=1:size(nodesToCheck2,1)
            dist=D1(nodesToCheck2(j,2),nodesToCheck2(j,1));
            if(dist<newDistance)
                newDistance=dist;
                [y_closest,x_closest]=find(D1==newDistance);
            end
        end
        
        if(newDistance>distLM | isempty(y_closest) | isempty(x_closest))
            break;
        end
        XY=[x_closest,y_closest];
        XY=XY(ismember(XY,nodesToCheck2,'rows'),:);
        x_closest=XY(1,1);
        y_closest=XY(1,2);
        
        
        %deletion
        skeletonOfParkingLot2(y_closest,x_closest)=0;
        %Check if it changes path between two original
        D2=bwdistgeodesic(skeletonOfParkingLot2,middleNode(1),middleNode(2),'quasi-euclidean');
        distance=D2(leftNode(2),leftNode(1));
        %get path between there nodes
        D = D1 + D2;
        D = round(D * 8) / 8;
        D(isnan(D)) = inf;
        skeleton_path = imregionalmin(D);
        %%getting all coordinates of the path
        [y_path,x_path]=find(skeleton_path==1);
        path=[x_path y_path];
        
        %if it has changed the distance between the 2 orginial nodes,
        %improvement found -> new right side
        newLeftNode=[x_closest,y_closest];
        
        if(distance~=distLM & ~isempty(newLeftNode) & ~ismember(rightNode,path,'rows'))
            leftNode=newLeftNode;
            distLM=newDistance;
            break;
            %else we delete the node and check for other possibilities
        else
            [~,ridx] = ismember(newLeftNode,nodesToCheck2,'rows');
            if(ridx(1)>0)
                nodesToCheck2(ridx(1),:)=[];
            end
        end
    end
    nodes(i,:)=[leftNode middleNode rightNode distLM distMR];
    
end









if(draw==1)

    figure
    imshow(skeletonOfParkingLot);
    hold on
    plot(crossings(:,1),crossings(:,2),'go');
    plot(pointsToReach(:,1),pointsToReach(:,2),'rx');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Graph creation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nodeVector=nodes(1,1:2);
G=graph;
DG=digraph;
for i=1:size(nodes,1)
    start=nodes(i,1:2);
    costSM=nodes(i,7);
    middle=nodes(i,3:4);
    costME=nodes(i,8);
    endP=nodes(i,5:6);
    [isStartMember,idxStart] = ismember(start,nodeVector,'rows');
    [isMiddleMember,idxMiddle] = ismember(middle,nodeVector,'rows');
    [isEndPMember,idxEndP] = ismember(endP,nodeVector,'rows');
    if(~isStartMember)
        nodeVector(end+1,:)=start;
        idxStart=size(nodeVector,1);
    end
    if(~isMiddleMember)
        nodeVector(end+1,:)=middle;
        idxMiddle=size(nodeVector,1);
    end
    if(~isEndPMember)
        nodeVector(end+1,:)=endP;
        idxEndP=size(nodeVector,1);
    end
    G=addedge(G,[idxStart idxMiddle],[idxMiddle idxEndP], [costSM costME]);
    DG=addedge(DG,[idxStart idxMiddle],[idxMiddle idxEndP]);
    DG=addedge(DG,[idxEndP idxMiddle],[idxMiddle idxStart]);
end
if(draw==1)
    figure; plot(G);
end
%delete multiple edges with the same weight
G=simplify(G);
end

